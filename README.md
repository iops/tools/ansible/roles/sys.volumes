Role Name
=========

SYSTEM management role for managing fs volumes on linux systems.


Requirements
------------
- Working with `root` as remote_user (required for base system setup)
- Ensure facts gathering is enabled on your playbook:
```yaml
- hosts: servers
  gather_facts: yes
```


Role Variables
--------------

### `run_mode`
controls the main behaviour of the role. Possible values are:
- **deploy**: install defined packages & settings to target hosts
- **remove**: uninstall defined packages & settings from target hosts

### `run_opts`
influences the behaviour of some (debugging) tasks. Possible values are (multiple values can be provided **commy-separated**):
- **skip_debugs**: explicitely skip debug infos that would shownby default
- **show_debugs**: explicitely show debug infos that would be hidden by default


Dependencies
------------

None.


Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: yes
  remote_user: root
  roles:
     - { role: this.role }
```


License
-------

BSD
